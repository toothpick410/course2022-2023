Get the number of inhabitants of Santiago de Compostela

prefix gp: <http://GP-onto.fi.upm.es/exercise2#>
select distinct ?s where {
    gp:Santiago_de_Compostela gp:hasInhabitantNumber ?s
}





" 300000 "^^<http://www.w3.org/2001/XMLSchema#integer>