Construct the RDF(S) graph that relates all touristic places with their provinces, using a new property ”isIn”

prefix gp: <http://GP-onto.fi.upm.es/exercise2#>
CONSTRUCT { ?l gp:isIn ?p }
WHERE {
 ?c rdfs:subClassOf gp:TouristicLocation.
 ?l a ?c.
 ?c gp:inProvince ?p
}